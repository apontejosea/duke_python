# Jose Aponte
# Pragma Analytics, Inc.
# from: 10:18pm
# to: 10:37pm

import json
import random


def run(lower_bound=0, upper_bound=500, total_numbers=10000):
    original_random_list = [random.randint(lower_bound, upper_bound)
                            for _ in range(total_numbers)]
    sorted_asc_list = sorted(original_random_list)
    sorted_desc_list = sorted_asc_list[::-1]
    squares_list = [num ** 2 for num in original_random_list]
    odd_numbers_or_divisible_by_9_list = []
    count_between_200_400_divisible_by_3 = 0
    unique_set = set()

    for num in original_random_list:
        if (not num % 2 == 0) or (num % 9 == 0):
            odd_numbers_or_divisible_by_9_list.append(num)

        if 200 <= num <= 400 and num % 3 == 0:
            count_between_200_400_divisible_by_3 += 1

        unique_set.add(num)

    print(f'Q1: Original List:\n{original_random_list}')
    print(
        f'Q2:\n'
        f' Ascending:\n{sorted_asc_list}'
        f' Descending:\n{sorted_desc_list}')
    print(
        f'Q3: Odd numbers and numbers divisible by 9\n'
        f'{odd_numbers_or_divisible_by_9_list}'
    )
    print(f'Q4: Count occurrences of numbers between 200 and 400 divisible by 3'
          f'{count_between_200_400_divisible_by_3}')
    print(f'Q5: Square of all numbers:\n{squares_list}')
    print(f'Q6: Unique numbers\n{list(unique_set)}')

    result = {
        'Q1': original_random_list,
        'Q2': {
            'sorted_ascending': sorted_asc_list,
            'sorted_descending': sorted_desc_list,
        },
        'Q3': odd_numbers_or_divisible_by_9_list,
        'Q4': count_between_200_400_divisible_by_3,
        'Q5': squares_list,
        'Q6': list(unique_set),
    }

    return result


if __name__ == '__main__':
    final_result = run(0, 500, 10000)
    with open('final_result.json', 'w') as f:
        f.write(json.dumps(final_result, indent=4))
